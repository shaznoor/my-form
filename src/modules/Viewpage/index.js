import React, { useEffect, useState } from 'react';
import { Table, Button } from 'react-bootstrap';
import axios from 'axios';
import { useDispatch } from 'react-redux';
import { preview } from '../../features/formSlice';
import { useHistory } from 'react-router-dom';

/* function getDataFromLocalStorage() {
  if (localStorage.getItem('user') === null) {
    return [];
  }else{
      const s1=localStorage.getItem('user')
      const s2=JSON.parse(s1);
      return s2;
  }
} */

const Viewpage = () => {
  const [input, setInput] = useState([]);
  const history=useHistory();

  useEffect(() => {
    async function getAllUser() {
      const res = await axios.get('http://localhost:4000/user');
      setInput(res.data);
    }
    getAllUser();
  }, []);

  async function deleteUser(e) {
    const id =
      e.target.parentElement.previousElementSibling.previousElementSibling
        .innerHTML;
    const res = await axios.delete(`http://localhost:4000/user/${id}`);
    if (res.status == 200) {
      window.location.reload();
    }
  }

  const dispatch = useDispatch();

  async function editUser(e) {
    const id = e.target.parentElement.previousElementSibling.innerHTML;
    const res = await axios.get(`http://localhost:4000/user/${id}`);
    dispatch(
      preview({
        name: res.data.name,
        email: res.data.email,
        mobile: res.data.mobile,
        gender: res.data.gender,
        tech: res.data.tech,
        id: res.data._id,
        edit: true,
      })
    );
    history.push('/');
  }

  return (
    <Table striped bordered hover variant="dark">
      <thead>
        <tr>
          <th>#</th>
          <th>Name</th>
          <th>Email</th>
          <th>Mobile</th>
          <th>Gender</th>
          <th>Technology</th>
        </tr>
      </thead>
      <tbody>
        {input.map((item, index) => {
          return (
            <tr key={index + 1}>
              <td>{index + 1}</td>
              <td>{item.name}</td>
              <td>{item.email}</td>
              <td>{item.mobile}</td>
              <td>{item.gender}</td>
              <td>{item.tech}</td>
              <td style={{ display: 'none' }}>{item._id}</td>
              <td>
                <Button onClick={editUser}>Edit</Button>
              </td>
              <td>
                <Button onClick={deleteUser}>Delete</Button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </Table>
  );
};

export default {
  routeProps: {
    path: '/viewpage',
    component: Viewpage,
  },
  name: 'View',
};
