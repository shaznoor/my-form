import Home from './Home';
import ViewPage from './Viewpage';

export default [
    Home,
    ViewPage,
];