function userEditFunction(props, input, register) {
  if (props.user == undefined) {
    return (
      <input
        name={input.name}
        className="input"
        type={input.type}
        value={input.value}
        ref={register}
      />
    );
  } else {
    if (input.type != 'radio' && input.type != 'checkbox') {
      return (
        <input
          name={input.name}
          className="input"
          type={input.type}
          defaultValue={props.user[input.name]}
          ref={register}
        />
      );
    } else if (input.type == 'radio') {
      if (props.user.gender == input.value) {
        return (
          <input
            name={input.name}
            className="input"
            type={input.type}
            value={input.value}
            defaultChecked="true"
            ref={register}
          />
        );
      } else {
        return (
          <input
            name={input.name}
            className="input"
            type={input.type}
            value={input.value}
            ref={register}
          />
        );
      }
    } else {
      if (props.user.tech.split(',').includes(input.value)) {
        return (
          <input
            name={input.name}
            className="input"
            type={input.type}
            value={input.value}
            defaultChecked="true"
            ref={register}
          />
        );
      } else {
        return (
          <input
            name={input.name}
            className="input"
            type={input.type}
            value={input.value}
            ref={register}
          />
        );
      }
    }
  }
}

export default userEditFunction;