import React from 'react';
import content from './details';
import './index.css';
import { useDispatch, connect } from 'react-redux';
import { preview } from '../../features/formSlice';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import 'yup-phone';
import { Button } from 'react-bootstrap';
import MyVerticallyCenteredModal from './popup';
import userEditFunction from './userEditFunction';


const schema = yup.object().shape({
  name: yup.string().required(),
  email: yup.string().required().email(),
  mobile: yup.string().required().phone(),
  gender: yup.string().required().nullable(),
  tech: yup.array().required().min(1),
});

const Home = (props) => {
  const [modalShow, setModalShow] = React.useState(false);

  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(schema),
  });

  const dispatch = useDispatch();

  const onSubmit = (e) => {
    setModalShow(true);
    if (props.user == undefined) {
      dispatch(
        preview({
          name: e.name,
          email: e.email,
          mobile: e.mobile,
          gender: e.gender,
          tech: e.tech.join(),
          id: null,
          edit: false,
        })
      );
    } else {
      if (props.user.edit == true) {
        dispatch(
          preview({
            name: e.name,
            email: e.email,
            mobile: e.mobile,
            gender: e.gender,
            tech: e.tech.join(),
            id: props.user.id,
            edit: true,
          })
        );
      } else {
        dispatch(
          preview({
            name: e.name,
            email: e.email,
            mobile: e.mobile,
            gender: e.gender,
            tech: e.tech.join(),
            id: null,
            edit: false,
          })
        );
      }
    }
  };

  return (
    <div className="form">
      <h1>FORM</h1>
      <form onSubmit={handleSubmit(onSubmit)}>
        {content.inputs.map((input, key) => {
          return (
            <div key={key}>
              <p>
                <label>{input.label}</label>
              </p>
              <p>{userEditFunction(props, input, register)}</p>
              <p className="message">{errors[input.name]?.message}</p>
            </div>
          );
        })}
        <>
          <Button
            variant="primary"
            type="submit"
            onClick={() => setModalShow(false)}
          >
            Preview
          </Button>

          <MyVerticallyCenteredModal
            show={modalShow}
            onHide={() => setModalShow(false)}
          />
        </>
      </form>
    </div>
  );
};

function mapStateToProps(state) {
  return {
    user: state.user.user,
  };
}

export default {
  routeProps: {
    path: '/',
    exact: true,
    component: connect(mapStateToProps)(Home),
  },
  name: 'Home',
};
