import { connect } from 'react-redux';
import { Modal, Button } from 'react-bootstrap';
import axios from 'axios';

/* function onSubmit(props) {
  function saveData() {
    let arr = [];
    if (localStorage.getItem('user') !== null) {
      arr = JSON.parse(localStorage.getItem('user'));
    }
    arr.push(props.user);
    const res = JSON.stringify(arr);
    localStorage.setItem('user', res);
    window.location.reload();
  }
  return saveData;
} */

function onSubmit(props) {
  if(props.user.edit==true){
    async function saveData() {
      const data = {
        id:props.user.id,
        name: props.user.name,
        email: props.user.email,
        mobile: props.user.mobile,
        gender: props.user.gender,
        tech: props.user.tech,
      };
      const res = await axios.put('http://localhost:4000/user', data);
      if (res.status == 200) {
        window.location.reload();
      }
    }
    return saveData;
  }else{
    async function saveData() {
      const data = {
        name: props.user.name,
        email: props.user.email,
        mobile: props.user.mobile,
        gender: props.user.gender,
        tech: props.user.tech,
      };
      const res = await axios.post('http://localhost:4000/user', data);
      if (res.status == 201) {
        window.location.reload();
      }
    }
    return saveData;
  }
}

export function MyVerticallyCenteredModal(props) {
  if (props.user !== null) {
    return (
      <Modal
        {...props}
        size="lg"
        backdrop="static"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">FORM</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>NAME: {props.user.name}</p>
          <p>EMAIL: {props.user.email}</p>
          <p>MOBILE: {props.user.mobile}</p>
          <p>GENDER: {props.user.gender}</p>
          <p>TECHNOLOGY:{' '}{props.user.tech}</p>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>Close</Button>
          <Button onClick={onSubmit(props)}>Submit</Button>
        </Modal.Footer>
      </Modal>
    );
  } else {
    return <></>;
  }
}

function mapStateToProps(state) {
  return {
    user: state.user.user,
  };
}

export default connect(mapStateToProps)(MyVerticallyCenteredModal);
