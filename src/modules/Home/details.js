const content = {
  inputs: [
    {
      label: 'Name',
      name: 'name',
      type: 'text',
    },
    {
      label: 'Email',
      name: 'email',
      type: 'email',
    },
    {
      label: 'Mobile',
      name: 'mobile',
      type: 'text',
    },
    {
      label: 'Male',
      name: 'gender',
      type: 'radio',
      value: 'Male',
    },
    {
      label: 'Female',
      name: 'gender',
      type: 'radio',
      value: 'Female',
    },
    {
      label: 'C',
      name: 'tech',
      type: 'checkbox',
      value: 'C',
    },
    {
      label: 'C++',
      name: 'tech',
      type: 'checkbox',
      value: 'C++',
    },
    {
      label: 'Java',
      name: 'tech',
      type: 'checkbox',
      value: 'Java',
    },
    {
      label: 'Javascript',
      name: 'tech',
      type: 'checkbox',
      value: 'Javascript',
    },
    {
      label: 'Python',
      name: 'tech',
      type: 'checkbox',
      value: 'Python',
    },
  ],
};

export default content;
