import { createSlice } from '@reduxjs/toolkit';

export const formSlice = createSlice({
  name: 'user',
  initialState: {
    user: null,
  },
  reducers: {
    preview: (state, action) => {
      state.user = action.payload;
    },
  },
});

export const { preview } = formSlice.actions;
export const selectUser = (state) => state.user.user;
export default formSlice.reducer;
